package com.atlassian.mywork.example;

import com.atlassian.mywork.service.RegistrationProvider;

public class ExampleRegistrationProvider implements RegistrationProvider
{
    public static final String ID = "com.atlassian.mywork.example";

    @Override
    public String getApplication()
    {
        return ID;
    }

    @Override
    public String getPackage()
    {
        return getClass().getPackage().getName();
    }

    @Override
    public String getPluginId()
    {
        return "com.atlassian.mywork.mywork-example-plugin";
    }
}
