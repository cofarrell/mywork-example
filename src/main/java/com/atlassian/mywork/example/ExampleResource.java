package com.atlassian.mywork.example;

import com.atlassian.applinks.host.spi.HostApplication;
import com.atlassian.mywork.model.Notification;
import com.atlassian.mywork.model.NotificationBuilder;
import com.atlassian.mywork.model.Task;
import com.atlassian.mywork.model.TaskBuilder;
import com.atlassian.mywork.service.NotificationService;
import com.atlassian.mywork.service.TaskService;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Produces({MediaType.APPLICATION_JSON})
@Path("/")
public class ExampleResource
{
    private final NotificationService notificationService;
    private final TaskService taskService;
    private final ExampleService exampleService;
    private final HostApplication hostApplication;

    public ExampleResource(NotificationService notificationService, TaskService taskService, ExampleService exampleService, HostApplication hostApplication)
    {
        this.notificationService = notificationService;
        this.taskService = taskService;
        this.exampleService = exampleService;
        this.hostApplication = hostApplication;
    }

    @GET
    @Path("count")
    public Response getCount()
    {
        return Response.ok(exampleService.getCount()).build();
    }

    @POST
    @Path("notification")
    public Response createNotification() throws Exception
    {
        ObjectNode metadata = new ObjectMapper().createObjectNode();
        metadata.put("user", "Some Body");
        metadata.put("username", "test");
        metadata.put("custom", "My Example");
        Notification notification = notificationService.createOrUpdate("admin", new NotificationBuilder()
                .application("com.atlassian.mywork.example")
                .title("Some title")
                .itemTitle("Top level title")
                .description("This has some example text")
                .entity("foo")
                .action("bar")
                .globalId("appId=" + hostApplication.getId().get() + "entity=foo&id=" + exampleService.getCount())
                .url("http://www.google.com/")
                .metadata(metadata)
                .applicationLinkId(hostApplication.getId().get())
                .createNotification()).get();
        return Response.ok(notification).build();
    }

    @POST
    @Path("task")
    public Response createTask() throws Exception
    {
        ObjectNode metadata = new ObjectMapper().createObjectNode();
        metadata.put("user", "Some Body");
        metadata.put("username", "test");
        Task task = taskService.createOrUpdate("admin", new TaskBuilder()
                .application("com.atlassian.mywork.example")
                .title("Some title")
                .itemTitle("Top level title")
                .notes("Some example text")
                .globalId("appId=" + hostApplication.getId().get() + "entity=foo&id=" + exampleService.getCount())
                .entity("foo")
                .url("http://www.google.com/")
                .metadata(metadata)
                .applicationLinkId(hostApplication.getId().get())
                .createTask()).get();
        return Response.ok(task).build();
    }
}
