package com.atlassian.mywork.example;

import com.atlassian.mywork.service.ActionResult;
import com.atlassian.mywork.service.ActionService;
import com.atlassian.mywork.service.NotificationService;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

public class ExampleActionService implements ActionService
{
    private final ExampleService exampleService;
    private final NotificationService notificationService;

    public ExampleActionService(final ExampleService exampleService, NotificationService notificationService)
    {
        this.exampleService = exampleService;
        this.notificationService = notificationService;
    }

    @Override
    public String getApplication()
    {
        return ExampleRegistrationProvider.ID;
    }

    @Override
    public ActionResult execute(String username, JsonNode json)
    {
        String qualifiedAction = json.get("qualifiedAction").getTextValue();
        if (qualifiedAction.equals("com.atlassian.mywork.example.foo.increment"))
        {
            exampleService.inc();

            markIncremented(username, json.get("globalId").getTextValue());
        }
        return ActionResult.SUCCESS;
    }

    private void markIncremented(String username, String globalId)
    {
        ObjectMapper objectMapper = new ObjectMapper();

        // Empty condition -> update all notifications with matching globalId
        ObjectNode condition = objectMapper.createObjectNode();

        // Mark notification incremented so that reply action (See registration-actions.json) will become available.
        ObjectNode metadata = objectMapper.createObjectNode();
        metadata.put("incremented", true);

        notificationService.updateMetadata(username, globalId, condition, metadata);
    }
}
