package com.atlassian.mywork.example;

public class ExampleService
{
    private volatile int count = 0;

    public void inc()
    {
        count++;
    }

    public int getCount()
    {
        return count;
    }
}
