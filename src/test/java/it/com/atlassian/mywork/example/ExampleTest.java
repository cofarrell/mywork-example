package it.com.atlassian.mywork.example;

import com.atlassian.mywork.model.Notification;
import com.atlassian.mywork.model.Task;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class ExampleTest
{
    private String url = "http://localhost:1990/confluence";

    private void deleteAllNotifications()
    {
        for (Notification notification : getNotifications())
        {
            createClient().path("/rest/mywork/1/notification/" + notification.getId()).delete();
        }
    }

    @Test
    public void testNotification()
    {
        deleteAllNotifications();
        createClient().path("/rest/mywork-example/1/notification").post();
        List<Notification> notifications = getNotifications();
        assertEquals(1, notifications.size());
    }

    private Integer getCount()
    {
        return createClient().path("/rest/mywork-example/1/count/").get(Integer.class);
    }

    @Test
    public void testTask()
    {
        createClient().path("/rest/mywork-example/1/task").post();
        List<Task> tasks = getTasks();
        assertEquals(1, tasks.size());
    }

    private List<Notification> getNotifications()
    {
        return createClient().path("/rest/mywork/1/notification").get(new GenericType<List<Notification>>()
        {
        });
    }

    private List<Task> getTasks()
    {
        return createClient().path("/rest/mywork/1/task").get(new GenericType<List<Task>>()
        {
        });
    }

    private WebResource createClient()
    {
        ClientConfig cc = new DefaultClientConfig();
        cc.getClasses().add(JacksonJsonProvider.class);
        Client client = Client.create(cc);
        client.addFilter(new HTTPBasicAuthFilter("admin", "admin"));
        return client.resource(url);
    }
}
